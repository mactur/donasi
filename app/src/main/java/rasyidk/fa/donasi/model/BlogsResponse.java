package rasyidk.fa.donasi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BlogsResponse {
    @SerializedName("hasil")
    @Expose
    private ArrayList<Blogs> hasil = null;

    public BlogsResponse() {

    }

    public BlogsResponse(ArrayList<Blogs> hasil) {
        super();
        this.hasil = hasil;
    }

    public ArrayList<Blogs> getHasil() {

        return hasil;
    }

    public void setHasil(ArrayList<Blogs> hasil) {
        this.hasil = hasil;
    }
}
