package rasyidk.fa.donasi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Blogs {
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("isi")
    @Expose
    private String desc;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("id_user")
    @Expose
    private String id_user;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Blogs(String judul, String desc, String foto, String id_user) {
        super();
        this.judul = judul;
        this.desc = desc;
        this.foto = foto;
        this.id_user = id_user;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }




}
