package rasyidk.fa.donasi.ui;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import rasyidk.fa.donasi.UserSession;
import rasyidk.fa.donasi.model.Blogs;
import rasyidk.fa.donasi.model.BlogsResponse;
import rasyidk.fa.donasi.network.NetworkApi;
import rasyidk.fa.donasi.network.NetworkInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter extends LoginView implements MainPresenterInterface {

    MainViewInterface mvi;
    private Blogs datas;
    String TAG = "MainPresenter";
    Context ctx;

    public MainPresenter(MainViewInterface mvi) {
        this.mvi = mvi;
    }

    public MainPresenter() {

    }

    public MainPresenter(Context context) {
        this.ctx = context;
    }

    @Override
    public void getBlogs() {
        getObservable().subscribeWith(getObserver());
    }

    @Override
    public void postBlogs(Blogs datas) {
        this.datas = datas;
        setObservable().subscribeWith(setObserver());

        Log.d(TAG, "total "+datas);
    }

    @Override
    public void requestLogin(String username, String password, final UserSession session) {
        NetworkApi.getRetrofit().create(NetworkInterface.class)
                .loginRequest(username, password)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonResult = new JSONObject(response.body().string());

                                if (jsonResult.getString("error").isEmpty()) {

                                    String key = jsonResult.getString("token");
                                    displaySukses(ctx, session, key);

                                } else {
                                    Log.d("hasil", jsonResult.getString("error"));
                                    showToast(ctx, "user atau password salah");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("debug", "onFailure "+t.toString());
                    }
                });
    }

    public Observable<BlogsResponse> getObservable(){
        return NetworkApi.getRetrofit().create(NetworkInterface.class)
                .getBlogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<BlogsResponse> getObserver() {
        return new DisposableObserver<BlogsResponse>() {

            @Override
            public void onNext(@NonNull BlogsResponse blogsResponse) {
                Log.d(TAG,"OnNext "+blogsResponse);
                mvi.displayBlogs(blogsResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.d(TAG,"Error "+e);
                e.printStackTrace();
                mvi.displayError("Error fetching Movie Data");
            }

            @Override
            public void onComplete() {
                Log.d(TAG,"Completed");
            }
        };
    }

    public Observable<Blogs> setObservable(){
        return NetworkApi.getRetrofit().create(NetworkInterface.class)
                .postBlogs(datas.getJudul(), datas.getDesc(), datas.getFoto())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<Blogs> setObserver() {
        return new DisposableObserver<Blogs>() {

            @Override
            public void onNext(@NonNull Blogs blogsResponse) {
                Log.d(TAG, "sukses");
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.d(TAG, "error "+e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG,"Completed");
            }
        };
    }

}
