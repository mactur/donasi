package rasyidk.fa.donasi.ui;

import java.util.ArrayList;

import rasyidk.fa.donasi.UserSession;
import rasyidk.fa.donasi.model.Blogs;

public interface MainPresenterInterface {
    void getBlogs();
    void postBlogs(Blogs datas);
    void requestLogin(String name, String password, UserSession session);
}
